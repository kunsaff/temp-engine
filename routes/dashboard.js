var express = require('express');
var router = express.Router();
const userData =[
  {
    username: 'player1',
    password: 'player1',
    firstName: 'player',
    lastName: 'one'
  },
  {
    username: 'player2',
    password: 'player2',
    firstName: 'player',
    lastName: 'two'
  },
  {
    username: 'player3',
    password: 'player3',
    firstName: 'player',
    lastName: 'three'
  }
]

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('dashboard/index', {userData});
});

module.exports = router;
