var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/homev1', (req, res) => {
  const title = "Homepage"
  const subtitle = "Welcome to Homepage"

  res.render('home', {title, subtitle})
})

router.get('/home', (req, res) => {
  res.render('home')
})

router.get('/about', (req, res) => {
  res.render('about')
})

router.get('/contact-us', (req, res) => {
  res.render('contactUs')
})


module.exports = router;
